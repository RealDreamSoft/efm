var express = require('express');  
var router = express();  
var bodyParser = require('body-parser');
router.use(bodyParser.json({ type: 'application/json' }));
var urlencodedParser = bodyParser.urlencoded({ extended: false });
router.use('/static', express.static('public'));
var session = require('express-session');
router.use(session({secret: 'ssshhhhh'}));
var path = require('path');
var request = require('request');
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/spandana', { useNewUrlParser: true });

var User = require('./models/User');
var branch = require('./models/branch');
var customer = require('./models/customer');

checkAuthkey = function(db,authKey,calBack1,callBack2){
    db.collection("users").findOne({"auth_key":authKey}, function(err, result) {
        if(!err){
            if(result)
                calBack1(result);
            else
                callBack2(err);
        }else
            callBack2(err);
    })
}

router.get('/', function (request, response) {
    if(request.session.user){
        response.render('home.ejs', {user:request.session.user });
    }else{
        response.redirect("/admin/login");
    }
})
router.get('/login', function (request, response) {
    response.render('login.ejs', {status_message:"" });
})
router.post('/login',urlencodedParser, async function (request, response) {
    //console.log(request.body);
    var resp = {};
    resp.status="success";
    User.findOne({username:request.body.username,password:request.body.password}, function(err, userInfo){
        if (err) {
           // next(err);
           resp.status = "failed";
           resp.message = JSON.stringify(err);
           //console.log(err);
           response.render('login.ejs', {status_message:"Server error!" }); 
        } else {
            // console.log(userInfo);
            if(userInfo){
                resp.message = JSON.stringify(userInfo);
                request.session.user = userInfo;
                response.redirect("/admin/dashboard");
            }else{
                response.render('login.ejs', {status_message:"Username or Password are incorrect!" });
            }
        }
    });
    
})
router.get('/logout', function (request, response) {
    request.session.destroy();
    response.render('login.ejs', {status_message:"Logout successfully!" });
})
router.get('/dashboard', function (request, response) {
    if(request.session.user){
        console.log(request.session.user);
        response.render('home.ejs', {user:request.session.user });
    }else{
        response.redirect("/admin/login");
    }
})
 router.post('/branch/save',function(request,response){
    console.log(request.body);
    run().catch(err => console.log(err));
    async function run() {
        var resp = {status:"fail"};
        console.log(request.body);        
        if(request.body._id){
            await branch.updateOne({_id: request.body._id},{$set: request.body});
            resp.response = request.body;
        }
        else resp.response = await (new branch(request.body)).save();
        response.send(JSON.stringify(resp));
    }
})   
router.get('/branch/form',function(request,response){
    console.log(request.body);
    run().catch(error => console.log(error.stack));
    
    async function run(){
        var resp={status: "success"};
       resp.response=await branch.find();
        response.send(JSON.stringify(resp)); 
    }
})
router.post('/user/save',function(request,response){
    console.log(request.body);
    run().catch(err => console.log(err));
    async function run() {
        var resp = {status:"success"};
        console.log(request.body);
        if(request.body._id){
            await User.updateOne({_id: request.body._id},{$set: request.body});
            resp.response = request.body;
        }
        else if(await User.findOne({username:request.body.username})){
            resp.status = 409;
            resp.message = "Username exist";
        }
        else
         resp.response = await (new User(request.body)).save();
        response.send(JSON.stringify(resp));
    }
})   

router.get('/user/list',function(request,response){
    console.log(request.body);
    run().catch(error => console.log(error.stack));
    
    async function run(){
        var resp={status: "success"};
       resp.response=await User.find();
        response.send(JSON.stringify(resp)); 
    }
})
router.post('/customer/save',function(request, response){
    // console.log(request.body);
    run().catch(err => console.log(err));
    async function run() {
        var resp = {status:"fail"};
        if(request.body._id){
            await customer.updateOne({_id: request.body._id},{$set: request.body});
            resp.response = request.body;
        }
        else
         resp.response = await (new customer(request.body)).save();
        response.send(JSON.stringify(resp))
        ;
    }
})
router.get('/customer/list',function(request,response){
    run().catch(error => console.log(error.stack));
    async function run() {
        var resp={};
        resp.status="success";
        resp.response=await customer.find();
        response.send(JSON.stringify(resp));
    }
})
module.exports = router;