// grab the things we need
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var customerSchema = new Schema({
  customer_id:{type:Number,required: [true, 'Customer ID required!']},
  first_name: {type: String, required: [true, 'First Name required!'], max: 100},
  last_name: {type: String, required: [true, 'Last Name required!'], max: 100 },
  age: {type:Number, required:[true,'Age required!']},
  parent:{type:String,required: [true, 'Parent required!']},
  parent_type:{type:String,enum: ['D/O', 'S/O','W/O'],required: [true, 'Parent Type required!']},
//   so:String,
//   wo:String,
  mobile: {type: Number,required: [true, 'Mobile required!']},
  aadhar: {type: Number,required: [true, 'Aadhar Number required!']},
  status:{type:String,enum: ['Active', 'In-active']},
  gender: {type: String, enum:['Male','Female','Others']},
  permanent_address:{type: String,required:[true, 'Permanent Address required!'],max:400},
  present_address:{type: String,required:[true, 'present Address required!'],max:400},
  created_at: Date,
  updated_at: { type: Date, default: Date.now() }
});
var Customer = mongoose.model('customer', customerSchema);
module.exports = Customer;
