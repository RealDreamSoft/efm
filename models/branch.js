var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var branchSchema = new Schema({
    branchname: {type: String},
    address: {type: String},
    status:{type:String,enum: ['Active', 'Inactive'],default:'Inactive'},
    created_at: Date,
    updated_at: { type: Date, default: Date.now() }
})

var Branch = mongoose.model('branch',branchSchema); 
module.exports = Branch;