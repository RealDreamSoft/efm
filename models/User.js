// grab the things we need
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema

var userSchema = new Schema({
  first_name: {type: String, required: [true, 'First Name required!'], max: 100},
  last_name: {type: String, required: [true, 'Last Name required!'], max: 100},
  mobile: {type: Number, required: [true, 'Mobile required!']},
  date_of_birth: {type: String},
  username: { type: String, required: [true, 'Username required!']},
  password: { type: String, required: [true, 'Password required!'] },
  user_type: { type: String, required: [true, 'type required!'],enum: ['Admin','PA','User','Voter','Dept','Asst_Dept','CI'],default:'User' },
  location: {type: String},
  gender: { type: String, enum:['Male','Female','Others']},
  mandal: {type: Schema.Types.ObjectId, ref: 'Mandals'},
  village: {type: Schema.Types.ObjectId, ref: 'Villages'},
  status: {type:String, required: [true, 'Status required!'],enum: ['NotVerified', 'Verified'],default:'NotVerified'},
  otp: String,
  token:String,
   created_at: Date,
   updated_at: { type: Date, default: Date.now() }
});
var User = mongoose.model('User', userSchema);
module.exports = User;