angular.module('CommonServices', [])

.run(function($rootScope, $state, $stateParams){
	$rootScope.$state = $state;
	$rootScope.$stateParams = $stateParams;
	$rootScope.$on('$stateChangeSuccess', function() {
		if(document.body)
		document.body.scrollTop = document.documentElement.scrollTop = 0;
	}); 
})  
.constant("Constants",{
	
})
.service('APIServices',function($http,Constants){
	this.savebranch= function(branch){
		return $http.post("/admin/branch/save",branch);
	}
    this.getbranch = function(){
		return $http.get("/admin/branch/form");
	}
	this.saveuser = function(user){
		return $http.post("/admin/user/save",user);
	}
	this.usersList = function(){
		return $http.get("/admin/user/list");
	}
	this.savecustomer=function(customer){
		return $http.post('/admin/customer/save',customer);
	}
	this.customersList=function(){
		return $http.get('/admin/customer/list');
	}
})
