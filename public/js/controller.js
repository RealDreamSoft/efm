angular.module('CommonControllers', [])

.controller('HeaderController', function($scope,$rootScope,$state,$routeParams,APIServices) {
	
	
})
.controller('DashBoardCtrl', function($scope,$rootScope,$state,$routeParams,APIServices) {
	
	
})
.controller('DashBoardCtrl', function ($scope, $rootScope, $state, $routeParams, APIServices) {
    $scope.complaints = [];
    $scope.dashboard = {};
    $scope.timeline = [];
    $scope.timelineLen = 1;
    $scope.initialLoading = function () {
        APIServices.dashboard().then(res => {
            $scope.dashboard = res.data.response;

            // $scope.complaints = res.data.response.complaints;
            // setTimeout(function(){
            // 	$('.table').DataTable();
            // },100);	
            //$scope.adminsettings='#adminSettin
            // $scope.viewall= function(){gs'
            // $('#adminSettings').DataTable();
            // $('.dropdown-menu dropdown-secondary').
            //	$scope.viewall();
            //}
        });
        APIServices.loadTimeline($scope.timelineLen).then(res => {
            console.log(res);
            var result = res.data.newsFeed;
            for(let keyObj in result){
                var obj = JSON.parse(keyObj);
                obj.files = result[keyObj];
                $scope.timeline.push(obj);
            }
            $scope.timelineLen += 1;
        });
    }
    
})
.controller('branchCtrl', function($scope,$rootScope,$state,$routeParams,APIServices) {
		$scope.branches=[];
        $scope.branch={};
        $scope.edit=function(x){
            $scope.branch=x;
            console.log( $scope.branch);
        }
        $scope.submit=function(branch){
            APIServices.savebranch(branch).then(res => {
            $scope.branches.push($scope.branch);
                //    $scope.initialdata();
                $scope.branch={};
                Swal.fire("Saved Successfully");
            })
        }
    APIServices.getbranch().then(res => {
        $scope.branches = res.data.response;
        console.log($scope.branches);
        $('#table').dataTable();
    })
       
})
.controller('UserCtrl', function ($scope, $rootScope, $state, $routeParams, APIServices) {
    $scope.user = {};
    $scope.users = [];
    $('#date_of_birth').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true
    }).on('change changeDate', function () {
        $scope.user.date_of_birth = this.value;
    });
    $scope.edit=function(user){
        $scope.user=user;
        console.log( $scope.user);
    }
    $scope.submit=function(user){                              
        APIServices.saveuser(user).then(res => {
            if(res.data.status==409) Swal.fire(res.data.message);
            else{
                $scope.users.push($scope.user);
                $scope.user={};
                Swal.fire("Saved Successfully");
            }
        })
    }
    APIServices.usersList().then(res => {
        $scope.users = res.data.response;
        console.log($scope.users);
        $('#table').dataTable();
    })
})
.controller('customerCtrl', function($scope,$rootScope,$state,$routeParams,APIServices) {
    $scope.customer={};
    $scope.cust=[];
    $scope.edit=function(data){
        $scope.customer=data;
    }
    $scope.saveCustomer=function(customer){
        APIServices.savecustomer(customer).then(res=>{
            // $scope.cust.push($scope.customer);
            console.log($scope.customer);
            $scope.customer={};
            Swal.fire("Saved successfully");
        })
    }
    APIServices.customersList().then(res=>{
        console.log(res);
        $scope.cust=res.data.response;
        console.log($scope.cust);
        //  alert();
        $('#table').dataTable();
    })
})