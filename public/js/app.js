var headerApp = angular.module('RootApp', ['ngRoute','ui.router','CommonControllers','CommonServices','MyDirectives'])
.config(function($stateProvider, $urlRouterProvider) {
	$stateProvider

	.state('dashboard', {
		url : '/dashboard',
		templateUrl : "/static/htmls/dashboard.html",
		controller : 'DashBoardCtrl'
	})
	.state('branch', {
		url : '/branch',
		templateUrl : "/static/htmls/admin settings/branch.html",
		controller : 'branchCtrl'
	})
	.state('User', {
		url : '/create',
		templateUrl : "/static/htmls/admin settings/user.html",
		controller : 'UserCtrl'
	})
	.state('customer', {
	    url : '/customer',
	    templateUrl : "/static/htmls/Admin settings/customer.html",
	    controller : 'customerCtrl'
	})
	$urlRouterProvider.otherwise("/dashboard");
})

.filter('to_trusted', ['$sce', function($sce){
	return function(text) {
		return $sce.trustAsHtml(text);
	};
}]);